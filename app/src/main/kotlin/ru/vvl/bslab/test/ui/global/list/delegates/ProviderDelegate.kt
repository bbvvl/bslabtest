package ru.vvl.bslab.test.ui.global.list.delegates

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_provider.*
import ru.vvl.bslab.test.R
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.entity.Provider
import ru.vvl.bslab.test.extension.inflate
import ru.vvl.bslab.test.ui.global.list.GiftAdapter

class ProviderDelegate(
    private val giftClickListener: (GiftCard) -> Unit
) : AdapterDelegate<MutableList<Provider>>() {

    override fun isForViewType(items: MutableList<Provider>, position: Int): Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_provider))

    override fun onBindViewHolder(
        items: MutableList<Provider>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) = (holder as ViewHolder).bind(items[position])


    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)

        (holder as ViewHolder).recycle()
    }

    private inner class ViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        private lateinit var provider: Provider

        private val adapter = GiftAdapter(giftClickListener)

        init {
            rvItemProviderGiftCards.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = this@ViewHolder.adapter
                setHasFixedSize(true)
            }
        }

        fun bind(provider: Provider) {
            this.provider = provider

            tvItemProviderTitle.text = provider.title
            adapter.setData(provider.cards)
        }

        fun recycle() {
            rvItemProviderGiftCards.adapter = null
            adapter.setData(emptyList())
        }
    }

}