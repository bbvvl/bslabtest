package ru.vvl.bslab.test.di.provider

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import toothpick.InjectConstructor
import javax.inject.Provider

@InjectConstructor
class GsonProvider : Provider<Gson> {

    override fun get(): Gson = GsonBuilder().create()
}