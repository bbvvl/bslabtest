package ru.vvl.bslab.test.di.module

import android.content.Context
import okhttp3.OkHttpClient
import ru.vvl.bslab.test.R
import ru.vvl.bslab.test.di.ServerPath
import ru.vvl.bslab.test.di.provider.OkHttpClientProvider
import ru.vvl.bslab.test.di.provider.ServerApiProvider
import ru.vvl.bslab.test.model.data.server.ServerApi
import toothpick.config.Module
import toothpick.ktp.binding.bind

class ServerModule(context: Context) : Module() {
    init {
        val serverUrl = context.getString(R.string.server_url)

        bind(OkHttpClient::class).toProvider(OkHttpClientProvider::class).singleton()

        bind(String::class).withName(ServerPath::class).toInstance(serverUrl)
        bind(ServerApi::class).toProvider(ServerApiProvider::class).singleton()
    }
}