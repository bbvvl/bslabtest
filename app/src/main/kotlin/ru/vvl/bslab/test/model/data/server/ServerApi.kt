package ru.vvl.bslab.test.model.data.server

import io.reactivex.Single
import retrofit2.http.GET
import ru.vvl.bslab.test.entity.Providers

interface ServerApi {

    @GET("/")
    fun getProviders(): Single<Providers>
}