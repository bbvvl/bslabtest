package ru.vvl.bslab.test.ui.global.list

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.ui.global.list.delegates.GiftDelegate

class GiftAdapter(
    giftClickListener: (GiftCard) -> Unit
) : AsyncListDifferDelegationAdapter<GiftCard>(
    DiffItemCallback(),
    GiftDelegate(giftClickListener)
) {

    fun setData(data: List<GiftCard>) {
        items = data.toList()
    }

    private class DiffItemCallback : DiffUtil.ItemCallback<GiftCard>() {

        override fun areItemsTheSame(oldItem: GiftCard, newItem: GiftCard): Boolean =
            newItem.id == oldItem.id

        override fun areContentsTheSame(oldItem: GiftCard, newItem: GiftCard): Boolean =
            newItem == oldItem
    }
}