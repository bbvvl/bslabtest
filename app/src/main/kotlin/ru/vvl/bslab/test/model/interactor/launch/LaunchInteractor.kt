package ru.vvl.bslab.test.model.interactor.launch

import ru.vvl.bslab.test.model.repository.launch.LaunchRepository
import toothpick.InjectConstructor

@InjectConstructor
class LaunchInteractor(private val launchRepository: LaunchRepository) {

    fun signInToSession() = launchRepository.signInToSession()
}