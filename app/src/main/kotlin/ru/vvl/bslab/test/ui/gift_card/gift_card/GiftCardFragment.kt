package ru.vvl.bslab.test.ui.gift_card.gift_card

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_gift_card.*
import moxy.ktx.moxyPresenter
import ru.vvl.bslab.test.R
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.presentation.gift_card.gift_card.GiftCardPresenter
import ru.vvl.bslab.test.presentation.gift_card.gift_card.GiftCardView
import ru.vvl.bslab.test.ui.global.BaseFragment
import toothpick.ktp.extension.getInstance

class GiftCardFragment : BaseFragment(), GiftCardView {

    override val layoutRes = R.layout.fragment_gift_card

    private val presenter by moxyPresenter<GiftCardPresenter> { scope.getInstance() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tbGiftCard.setNavigationOnClickListener { onBackClicked() }
    }

    override fun setGiftCard(giftCard: GiftCard) {
        tbGiftCard.title = giftCard.title
        Glide
            .with(ivGift)
            .load(giftCard.imageUrl)
            .centerCrop()
            .into(ivGift)
        tvGiftPrice.text = giftCard.title.substringBefore(" ")
        tvGiftCoins.text = giftCard.credits.toString()
        tvGiftCardDescription.text = giftCard.description
    }

    override fun onBackClicked() {
        presenter.onBackClicked()
    }
}