package ru.vvl.bslab.test.entity

import com.google.gson.annotations.SerializedName

data class Provider(
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: String,
    @SerializedName("image_url") val image_url: String,
    @SerializedName("gift_cards") val cards: List<GiftCard>
)