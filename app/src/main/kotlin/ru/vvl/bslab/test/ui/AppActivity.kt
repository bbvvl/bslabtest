package ru.vvl.bslab.test.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import moxy.MvpAppCompatActivity
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import ru.vvl.bslab.test.R
import ru.vvl.bslab.test.di.DI
import ru.vvl.bslab.test.presentation.AppLauncher
import ru.vvl.bslab.test.ui.global.BaseFragment
import toothpick.ktp.KTP
import toothpick.ktp.binding.bind
import toothpick.ktp.binding.module
import toothpick.ktp.delegate.inject

class AppActivity : MvpAppCompatActivity() {

    private val appLauncher: AppLauncher by inject()
    private val navigatorHolder: NavigatorHolder by inject()

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(R.id.container) as? BaseFragment

    private val navigator: Navigator =
        object : SupportAppNavigator(
            this,
            supportFragmentManager,
            R.id.container
        ) {
            override fun setupFragmentTransaction(
                command: Command?,
                currentFragment: Fragment?,
                nextFragment: Fragment?,
                fragmentTransaction: FragmentTransaction
            ) {
                // fix incorrect order lifecycle callback of MainFlowFragment
                fragmentTransaction.setReorderingAllowed(true)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (!KTP.isScopeOpen(DI.LAUNCHER_SCOPE))
            KTP.openScopes(DI.APP_SCOPE, DI.LAUNCHER_SCOPE).apply {
                installModules(module { bind(AppLauncher::class).singleton() })
            }

        KTP.openScope(DI.LAUNCHER_SCOPE).inject(this)
        appLauncher.onLaunch()

        super.onCreate(savedInstanceState)

        setContentView(R.layout.layout_container)

        if (savedInstanceState == null) {
            appLauncher.coldStart()
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()

        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()

        super.onPause()
    }

    override fun onBackPressed() {
        currentFragment?.onBackClicked() ?: super.onBackPressed()
    }
}