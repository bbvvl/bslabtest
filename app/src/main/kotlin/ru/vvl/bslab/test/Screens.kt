package ru.vvl.bslab.test

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.vvl.bslab.test.ui.gift_card.GiftCardFlowFragment
import ru.vvl.bslab.test.ui.gift_card.gift_card.GiftCardFragment
import ru.vvl.bslab.test.ui.main.MainFlowFragment
import ru.vvl.bslab.test.ui.main.main.MainFragment
import kotlin.reflect.KClass

object Flow {
    object Main : FlowAppScreen(MainFlowFragment::class)

    data class GiftCard(
        private val giftCard: ru.vvl.bslab.test.entity.GiftCard
    ) : SupportAppScreen() {
        override fun getFragment() = GiftCardFlowFragment.newInstance(giftCard)
    }
}

object Screens {

    object Main {
        object Main : AppScreen(MainFragment::class)
    }

    object GiftCard {
        object GiftCard : AppScreen(GiftCardFragment::class)
    }
}

abstract class FlowAppScreen(
    private val flowFragment: KClass<out Fragment>
) : SupportAppScreen() {

    override fun getFragment() = flowFragment.java.newInstance()
}

abstract class AppScreen(
    private val fragment: KClass<out Fragment>
) : SupportAppScreen() {

    override fun getFragment() = fragment.java.newInstance()
}