package ru.vvl.bslab.test.presentation.gift_card.gift_card

import moxy.InjectViewState
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.model.system.flow.FlowRouter
import ru.vvl.bslab.test.presentation.global.BasePresenter
import toothpick.InjectConstructor

@InjectViewState
@InjectConstructor
class GiftCardPresenter(
    private val giftCard: GiftCard,
    private val flowRouter: FlowRouter
) : BasePresenter<GiftCardView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.setGiftCard(giftCard)
    }

    fun onBackClicked() {
        flowRouter.exit()
    }
}