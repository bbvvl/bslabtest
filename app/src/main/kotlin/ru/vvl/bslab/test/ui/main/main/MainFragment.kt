package ru.vvl.bslab.test.ui.main.main

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.*
import moxy.ktx.moxyPresenter
import ru.vvl.bslab.test.R
import ru.vvl.bslab.test.entity.Provider
import ru.vvl.bslab.test.extension.visible
import ru.vvl.bslab.test.presentation.main.main.MainPresenter
import ru.vvl.bslab.test.presentation.main.main.MainView
import ru.vvl.bslab.test.ui.global.BaseFragment
import ru.vvl.bslab.test.ui.global.list.ProviderAdapter
import toothpick.ktp.extension.getInstance

class MainFragment : BaseFragment(), MainView {

    override val layoutRes = R.layout.fragment_main

    private val presenter by moxyPresenter<MainPresenter> { scope.getInstance() }

    private val adapter by lazy { ProviderAdapter { presenter.onGiftCardClicked(it) } }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvMain.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = this@MainFragment.adapter
            setHasFixedSize(true)
        }
    }

    override fun setProviders(providers: List<Provider>) {
        adapter.setData(providers)
    }

    override fun showProgress(show: Boolean) {
        pbMain.visible(show)
    }

    override fun onBackClicked() {
        presenter.onBackClicked()
    }

    override fun onDestroyView() {
        super.onDestroyView()

        rvMain.adapter = null
    }

}