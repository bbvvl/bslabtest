package ru.vvl.bslab.test.presentation.gift_card.gift_card

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import ru.vvl.bslab.test.entity.GiftCard

@StateStrategyType(AddToEndSingleStrategy::class)
interface GiftCardView : MvpView {

    fun setGiftCard(giftCard: GiftCard)
}