package ru.vvl.bslab.test.presentation.global

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter
import moxy.MvpView

open class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    private val compositeDisposable = CompositeDisposable()

    fun Disposable.connect() = compositeDisposable.add(this)

    override fun onDestroy() {
        super.onDestroy()

        compositeDisposable.dispose()
    }
}