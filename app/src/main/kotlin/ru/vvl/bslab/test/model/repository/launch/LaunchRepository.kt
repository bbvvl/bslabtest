package ru.vvl.bslab.test.model.repository.launch

import android.content.Context
import ru.vvl.bslab.test.di.DI
import ru.vvl.bslab.test.di.module.ServerModule
import toothpick.InjectConstructor
import toothpick.ktp.KTP

@InjectConstructor
class LaunchRepository(private val context: Context) {

    fun signInToSession() {
        if (!KTP.isScopeOpen(DI.SERVER_SCOPE)) {
            KTP.openScopes(DI.LAUNCHER_SCOPE, DI.SERVER_SCOPE).installModules(ServerModule(context))
        }
    }
}