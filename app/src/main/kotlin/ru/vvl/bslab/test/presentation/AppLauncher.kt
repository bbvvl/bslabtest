package ru.vvl.bslab.test.presentation

import ru.terrakok.cicerone.Router
import ru.vvl.bslab.test.Flow
import ru.vvl.bslab.test.model.interactor.launch.LaunchInteractor
import toothpick.InjectConstructor

@InjectConstructor
class AppLauncher(
    private val launchInteractor: LaunchInteractor,
    private val router: Router
) {

    fun onLaunch() = launchInteractor.signInToSession()

    fun coldStart() = router.newRootScreen(Flow.Main)
}