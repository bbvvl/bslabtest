package ru.vvl.bslab.test.entity

import com.google.gson.annotations.SerializedName

data class Providers(
    @SerializedName("providers") val providers: List<Provider>
)