package ru.vvl.bslab.test.ui.gift_card

import android.os.Bundle
import ru.terrakok.cicerone.Router
import ru.vvl.bslab.test.Screens
import ru.vvl.bslab.test.di.DI
import ru.vvl.bslab.test.di.module.FlowNavigationModule
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.extension.setLaunchScreen
import ru.vvl.bslab.test.ui.global.FlowFragment
import toothpick.Scope
import toothpick.ktp.binding.bind
import toothpick.ktp.binding.module
import toothpick.ktp.delegate.inject
import toothpick.ktp.extension.getInstance


class GiftCardFlowFragment : FlowFragment() {

    override val parentScopeName = DI.SERVER_SCOPE

    override val scopeModuleInstaller = { scope: Scope ->
        scope.installModules(
            FlowNavigationModule(scope.getInstance()),
            module {
                bind(GiftCard::class).toInstance(giftCard)
            })
    }

    private val router: Router by inject()

    private val giftCard: GiftCard
        get() = arguments!!.getParcelable(ARG_GIFT_CARD)!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        scope.inject(this)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.GiftCard.GiftCard)
        }
    }

    override fun onExit() {
        router.exit()
    }

    companion object {
        private const val ARG_GIFT_CARD = "arg gift card"

        fun newInstance(giftCard: GiftCard) = GiftCardFlowFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_GIFT_CARD, giftCard)
            }
        }
    }
}