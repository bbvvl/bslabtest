package ru.vvl.bslab.test.model.repository.provider

import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.entity.Provider
import ru.vvl.bslab.test.model.data.server.ServerApi
import ru.vvl.bslab.test.model.system.SchedulersProvider
import toothpick.InjectConstructor

@InjectConstructor
class ProviderRepository(
    private val serverApi: ServerApi,
    private val schedulers: SchedulersProvider
) {

    fun getProviders() =
        serverApi
            .getProviders()
            .map { it.providers }
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
}