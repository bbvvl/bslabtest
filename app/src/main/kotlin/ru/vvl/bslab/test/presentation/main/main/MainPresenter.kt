package ru.vvl.bslab.test.presentation.main.main

import moxy.InjectViewState
import ru.vvl.bslab.test.Flow
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.model.interactor.provider.ProviderInteractor
import ru.vvl.bslab.test.model.system.flow.FlowRouter
import ru.vvl.bslab.test.presentation.global.BasePresenter
import toothpick.InjectConstructor

@InjectConstructor
@InjectViewState
class MainPresenter(
    private val flowRouter: FlowRouter,
    private val providerInteractor: ProviderInteractor
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        providerInteractor
            .getProviders()
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe({
                viewState.setProviders(it)
            }, {
                it.printStackTrace()
            })
            .connect()
    }

    fun onGiftCardClicked(giftCard: GiftCard){
        flowRouter.startFlow(Flow.GiftCard(giftCard))
    }

    fun onBackClicked() {
        flowRouter.exit()
    }
}