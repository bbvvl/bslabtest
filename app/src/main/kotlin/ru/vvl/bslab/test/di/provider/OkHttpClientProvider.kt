package ru.vvl.bslab.test.di.provider

import okhttp3.OkHttpClient
import ru.vvl.bslab.test.BuildConfig
import toothpick.InjectConstructor
import java.util.concurrent.TimeUnit
import javax.inject.Provider

@InjectConstructor
class OkHttpClientProvider() : Provider<OkHttpClient> {

    override fun get(): OkHttpClient = with(OkHttpClient.Builder()) {
        connectTimeout(TIMEOUT, TimeUnit.SECONDS)
        readTimeout(TIMEOUT, TimeUnit.SECONDS)

        build()
    }

    companion object {
        private const val TIMEOUT = 30L
    }
}