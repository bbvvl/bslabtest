package ru.vvl.bslab.test.ui.global.list.delegates

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_gift.*
import ru.vvl.bslab.test.R
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.extension.inflate

class GiftDelegate(
    private val giftClickListener: (GiftCard) -> Unit
) : AdapterDelegate<MutableList<GiftCard>>() {

    override fun isForViewType(items: MutableList<GiftCard>, position: Int): Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_gift))

    override fun onBindViewHolder(
        items: MutableList<GiftCard>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) = (holder as ViewHolder).bind(items[position])

    private inner class ViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        private lateinit var giftCard: GiftCard

        init {
            containerView.setOnClickListener { giftClickListener(giftCard) }
        }

        fun bind(giftCard: GiftCard) {
            this.giftCard = giftCard

            Glide
                .with(ivItemGift)
                .load(giftCard.imageUrl)
                .centerCrop()
                .into(ivItemGift)
            tvItemGiftPrice.text = giftCard.title.substringBefore(" ")
            tvItemGiftCoins.text = giftCard.credits.toString()
        }
    }
}