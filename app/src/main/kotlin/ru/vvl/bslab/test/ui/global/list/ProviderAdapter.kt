package ru.vvl.bslab.test.ui.global.list

import androidx.recyclerview.widget.DiffUtil
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import ru.vvl.bslab.test.entity.GiftCard
import ru.vvl.bslab.test.entity.Provider
import ru.vvl.bslab.test.ui.global.list.delegates.ProviderDelegate

class ProviderAdapter(
    giftClickListener: (GiftCard) -> Unit
) : AsyncListDifferDelegationAdapter<Provider>(
    DiffItemCallback(),
    ProviderDelegate(giftClickListener)
) {

    fun setData(data: List<Provider>) {
        items = data.toList()
    }

    private class DiffItemCallback : DiffUtil.ItemCallback<Provider>() {

        override fun areItemsTheSame(oldItem: Provider, newItem: Provider): Boolean =
            newItem.id == oldItem.id

        override fun areContentsTheSame(oldItem: Provider, newItem: Provider): Boolean =
            newItem == oldItem
    }
}