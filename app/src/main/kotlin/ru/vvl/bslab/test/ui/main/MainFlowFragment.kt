package ru.vvl.bslab.test.ui.main

import android.os.Bundle
import ru.terrakok.cicerone.Router
import ru.vvl.bslab.test.Screens
import ru.vvl.bslab.test.di.DI
import ru.vvl.bslab.test.di.module.FlowNavigationModule
import ru.vvl.bslab.test.extension.setLaunchScreen
import ru.vvl.bslab.test.ui.global.FlowFragment
import toothpick.Scope
import toothpick.ktp.delegate.inject
import toothpick.ktp.extension.getInstance

class MainFlowFragment : FlowFragment() {

    override val parentScopeName = DI.SERVER_SCOPE

    override val scopeModuleInstaller = { scope: Scope ->
        scope.installModules(FlowNavigationModule(scope.getInstance()))
    }

    private val router: Router by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        scope.inject(this)

        if (childFragmentManager.fragments.isEmpty()) {
            navigator.setLaunchScreen(Screens.Main.Main)
        }
    }

    override fun onExit() {
        router.exit()
    }
}