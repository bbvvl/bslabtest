package ru.vvl.bslab.test.di

object DI {
    const val APP_SCOPE = "app scope"
    const val LAUNCHER_SCOPE = "launcher scope"
    const val SERVER_SCOPE = "server scope"
}