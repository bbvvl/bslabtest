package ru.vvl.bslab.test.model.interactor.provider

import ru.vvl.bslab.test.model.repository.provider.ProviderRepository
import toothpick.InjectConstructor

@InjectConstructor
class ProviderInteractor(
    private val providerRepository: ProviderRepository
) {

    fun getProviders() = providerRepository.getProviders()
}