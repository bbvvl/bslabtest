package ru.vvl.bslab.test.di.provider

import ru.vvl.bslab.test.model.data.server.ServerApi
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.vvl.bslab.test.di.ServerPath
import toothpick.InjectConstructor
import javax.inject.Provider

@InjectConstructor
class ServerApiProvider(
    private val okHttpClient: OkHttpClient,
    private val gson: Gson,
    @ServerPath private val serverUrl: String
) : Provider<ServerApi> {

    override fun get(): ServerApi =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(serverUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ServerApi::class.java)
}