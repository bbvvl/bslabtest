package ru.vvl.bslab.test


import android.app.Application
import ru.vvl.bslab.test.di.DI
import ru.vvl.bslab.test.di.module.AppModule
import toothpick.configuration.Configuration
import toothpick.ktp.KTP

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        initDebugDependencies()
        initAppScope()
    }

    private fun initDebugDependencies() {
        KTP.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
    }

    private fun initAppScope() {
        KTP.openScope(DI.APP_SCOPE).apply {
            installModules(AppModule(this@App))
        }
    }
}