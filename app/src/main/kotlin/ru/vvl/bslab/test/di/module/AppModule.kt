package ru.vvl.bslab.test.di.module

import android.content.Context
import com.google.gson.Gson
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.vvl.bslab.test.di.provider.GsonProvider
import ru.vvl.bslab.test.model.system.AppSchedulers
import ru.vvl.bslab.test.model.system.SchedulersProvider
import toothpick.config.Module
import toothpick.ktp.binding.bind


class AppModule(context: Context) : Module() {
    init {
        bind(Context::class).toInstance(context)
        bind(SchedulersProvider::class).toInstance(AppSchedulers())
        bind(Gson::class).toProvider(GsonProvider::class).providesSingleton()

        val cicerone = Cicerone.create()
        bind(Router::class).toInstance(cicerone.router)
        bind(NavigatorHolder::class).toInstance(cicerone.navigatorHolder)


    }
}