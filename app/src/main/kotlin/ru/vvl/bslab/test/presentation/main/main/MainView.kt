package ru.vvl.bslab.test.presentation.main.main

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType
import ru.vvl.bslab.test.entity.Provider

@StateStrategyType(AddToEndSingleStrategy::class)
interface MainView : MvpView {

    fun setProviders(providers: List<Provider>)

    fun showProgress(show: Boolean)
}