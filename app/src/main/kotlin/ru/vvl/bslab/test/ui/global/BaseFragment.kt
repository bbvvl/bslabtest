package ru.vvl.bslab.test.ui.global

import ru.vvl.bslab.test.extension.objectScopeName
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import moxy.MvpAppCompatFragment
import toothpick.Scope
import toothpick.ktp.KTP

abstract class BaseFragment : MvpAppCompatFragment() {
    abstract val layoutRes: Int

    protected var instanceStateSaved: Boolean = false

    private val viewHandler = Handler()

    protected open val parentScopeName: String by lazy {
        (parentFragment as? BaseFragment)?.fragmentScopeName
            ?: throw RuntimeException("Must be parent fragment!")
    }

    protected open val scopeModuleInstaller: (Scope) -> Scope = { it }

    lateinit var fragmentScopeName: String
    protected lateinit var scope: Scope
        private set

    override fun onCreate(savedInstanceState: Bundle?) {
        fragmentScopeName = savedInstanceState?.getString(STATE_SCOPE_NAME) ?: objectScopeName()

        if (KTP.isScopeOpen(fragmentScopeName)) {
            scope = KTP.openScope(fragmentScopeName)
        } else {
            scope = KTP.openScopes(parentScopeName, fragmentScopeName)
            scopeModuleInstaller(scope)
        }

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(layoutRes, container, false)

    override fun onStart() {
        instanceStateSaved = false
        super.onStart()
    }

    override fun onResume() {
        instanceStateSaved = false
        super.onResume()
    }

    // fix for async views (like swipeToRefresh and RecyclerView)
    // if synchronously call actions on swipeToRefresh in sequence show and hide then swipeToRefresh will not hidden
    protected fun postViewAction(action: () -> Unit) {
        viewHandler.post(action)
    }

    open fun postViewActionDelayed(delay: Long = 300L, action: () -> Unit) {
        viewHandler.postDelayed(action, delay)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHandler.removeCallbacksAndMessages(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
        outState.putString(STATE_SCOPE_NAME, fragmentScopeName)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (needCloseScope()) {
            // destroy this fragment with scope
            KTP.closeScope(scope.name)
        }
    }

    // It will be valid only for 'onDestroy()' method
    private fun needCloseScope(): Boolean =
        when {
            activity?.isChangingConfigurations == true -> false
            activity?.isFinishing == true -> true
            else -> isRealRemoving()
        }

    fun isRealRemoving(): Boolean =
        (isRemoving && !instanceStateSaved) || // because isRemoving == true for fragment in backstack on screen rotation
                ((parentFragment as? BaseFragment)?.isRealRemoving() ?: false)

    open fun onBackClicked() {}

    companion object {
        private const val STATE_SCOPE_NAME = "state_scope_name"
        private const val PROGRESS_DIALOG_TAG = "progress_dialog_tag"
    }
}