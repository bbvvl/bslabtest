# Q&A
- как организовать возможность смены стиля отображения карт?

В этой реализации я отрисовываю данные из ответа сервера как есть, но их можно сконвертировать в нужный формат на основе каких то параметров и для каждого из новых форматов можно написать собственный AdapterDelegate и отрисовывать именно так как это необходимо (будь то полностью другой UI карточки или изменение цветовой палитры и т.п.)

- как открывать детализацию определенной карты по получению PUSH-уведомления?

Получаем пуш, отрисовывем нотификацию, которая вызывает onNewIntent в activity и внутри https://bitbucket.org/bbvvl/bslabtest/src/master/app/src/main/kotlin/ru/vvl/bslab/test/presentation/main/main/MainPresenter.kt#lines-34

- как минимальными изменениями в коде загружать данные с другого сервера (формат тот же)

https://bitbucket.org/bbvvl/bslabtest/src/master/app/src/main/res/values/strings.xml#lines-3

-  как при этом организовать кеширование для загружаемых данных?

если достаточно кеша пока запущено приложение можно использовать in-memory кеш (например такой https://gitlab.com/terrakok/gitlab-client/blob/develop/app/src/main/java/ru/terrakok/gitlabclient/model/data/cache/ProjectCache.kt).
Если же нужен кеш и после перезапуска приложения, то достаточно выбрать необходимый в данном случае или который требует бизнес (synchronized, lazy , lru и  т.д.)

